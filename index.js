// TIE WITH HTML COMPONENT
let button = [].slice.call(document.getElementsByClassName("button"));
let result = document.getElementById("result");
let enemyA = document.getElementById("enemyA");
let enemyB = document.getElementById("enemyB");
let enemyC = document.getElementById("enemyC");
let startbutton = document.getElementById("startlistening");
let stopbutton = document.getElementById("stoplistening");

result.onclick = reset

console.log(button)

// ABSTRACT CLASS - ABSTRACTION
class Player {
    constructor(name, isHuman) {
        this.name = name;
        this.isHuman = isHuman;
    };
    clicking(value) {
        console.log(value)
    }
    random() {
        let value = [4,5,6]
        return value[Math.floor(Math.random()*value.length)]
    }
}

// CHILD CLASS - INHERITANCE
class MainPlayer extends Player {
    constructor(name, isHuman) {
        super(name, isHuman)
    }

    // PRIVATE FUNCTION - ENCAPSULATION
    #cheat() {
        console.log("cheating")
    }
}

// CHILD CLASS - INHERITANCE
class Robot extends Player {
    constructor(name, isHuman) {
        super(name, isHuman) 
    }

    //OVERLOADING - Polymorphism
    random(value) {
        return value[Math.floor(Math.random()*value.length)]
    }
}

let player1 = new MainPlayer("Ahmad", isHuman=true);
let computer = new Robot("Smart Computer", isHuman=false);

const diceArray = [1,2,3]
let userSelection = 1

// USER CLICKING BUTTON
button.forEach((element, index) => {
    var selectedIndex;
    element.onclick = (v) => {
        userSelection = v.target.value
        selectedIndex = index
        for (let i = 0; i < button.length; i++) {
            const element = button[i];
            if (i !== selectedIndex) {
                element.style.backgroundColor = "green"
                element.disabled = true
            }
        }
        element.style.backgroundColor = "red"
        let enemyChoosen = computer.random(diceArray);
        result.textContent = getWinner(userSelection, enemyChoosen)

    }

});

// GET STATUS
function getWinner(firstPlayer, secondPlayer) {
    var status;
    if(firstPlayer > secondPlayer) {
        status = "WIN"
    } else if (firstPlayer < secondPlayer) {
        status = "LOSE"
    } else {
        status = "DRAW"
    }
    return status
}

// function startListening() {
//     result.addEventListener("click", reset);
// }

// function stopListening() {
//     console.log("stop")
//     result.removeEventListener("click", reset);
// }

// startbutton.onclick = () => {
//     startListening()
// }

// stopbutton.onclick = () => {
//     stopListening()
// }

function reset() {
    userSelection = 0
    result.textContent = "RESULT"
    button.forEach((element) => {
        console.log(element.value)
        element.disabled = false
        element.style.backgroundColor = "black"
    })
}





